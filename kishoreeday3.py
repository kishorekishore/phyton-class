#AND operators
a=1000
b=2000
print(a<b and b>a)
print()

#or operators
a=100
b=300
print(a!=b or a==b)
print(a>b or b<a)
print()

#NOT operators
num1=300
num2=500
a=(num1>num2)
not a
print(a)
print()

#IN operators
word= "noel is a good boy"
print("noel" in word)
print('kishore' in word)
print()

# if,elif,else control flow
a=100
b=200
c=300
if a>=b:
    print("The statement is correct")
elif c==b:
      print("The statement is correct")
else:
      print("The statement is incorrect")
print()


#input() functions

#name=input("enter your name")
#print("HI",name)
#print()

#phyton loops
#for loop
name="kishore"
#for lap in name  :
 #   print(lap)
for lap in name:
        if lap =='o':
             print("o is presnt")
             break

        else:
            print("o is not present")
            print()
print()
for lap in name:
        if lap =='o':
            continue
           # print("o is presnt")

        else:
            print("o is not present")
print()

#while loop

i=1
while i<4:
    print(i)
    i += 1
    if i==3:
        break
        print('looping ended')


#range
var=int(input("enter the number"))
for i in range(0,var):
     print(i)
     print()

for i in range(0,30,3):
    print(i)
else:
    print("all the given num are fineshed")

#functions in pyton
#def
def addition(a,b):
    print(a + b)

def subarction(a,b):
    print(a - b)

#return
def key(a):
   return a*100

addition(10,30)
subarction(26,76)
print(key(9))

#function arguments
def nam(name='hgj'):
    print("welcome"+name)

nam("kishore")
nam('lassy')
nam()
print()

#list comprehension

a=[1,2,3,4,5,6]
b=[]

for i in a:
 c=i ** 2
 b.append(c)
print(b)


list_square_new=[square_new **2 for square_new in a ]
print(list_square_new)
print()

#generators


def evens(start,stop):
    for even in range(start,stop+1,2):
        yield(even)
def main():
    even_values=[even for even in evens(20,40)]
    even2=tuple(evens(20,30))
    print(even_values)
    print(even2)
m=main()
g=evens(4,10)
print(next(g))
print(next(g))
print(next(g))
print(next(g))

print(list(g))
g1=evens(2,8)
print(list(g1))

g2=evens(8,20)
for x in g2:
    print(x)

print()

#module

import sys
sys.path.append(r"C:\Users\kishore\Desktop\phyton.assignment\.idea")
import pip as  kissy

kissy.mode(4)
print()



