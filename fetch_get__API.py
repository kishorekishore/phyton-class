# required packages
import requests
import pymongo

# required variables
pyr = open(r'python.txt', 'r')
pyw = open(r'python.txt', 'w')
list_of_chars = ['{logging_mixin.py:109} INFO -']

# HTTRequest for fetch an LOG API
res = requests.get(
    "http://128.199.26.95:8080/api/v1/dags/Star_Pyramid/dagRuns/Star_Pyramid_M00039_2021-10-27_14:01:08.317948+00:00/taskInstances/print_the_pyramid/logs/1"
    , auth=('airflow', "airflow"))
api = res.text

# Remove multiple characters from the string

for character in list_of_chars:
    run = api.replace(character, ' ')


    # appending the removed text for further
    # creating a new txt file

    with pyw as fd:
        fd.write(run)

    # list to store file lines
    lines = []
    # read file
    with pyr as fp:
        # read an store all lines into list
        lines = fp.readlines()
    # removing the unwanted lines
    # Write file
    with open(r'python.txt', 'w') as fp:
        # iterate each line
        for number, line in enumerate(lines):

            # delete line 0 and 8. or pass any Nth line you want to remove
            # note list index starts from 0
            if number not in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 30, 31, 32, 33]:
                fp.write(line)
                print(line)

myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["LOGGGS"]
mycol = mydb["THE FINAL LOGSS LINE"]

# open a file
f = open('python.txt')

# read the entire contents, should be UTF-8 text
text = f.read()

# build a document to be inserted
text_file_doc = {"file_name": "python.txt", "contents": text}

# insert the contents into the "file" collection
mycol.insert_one(text_file_doc)


